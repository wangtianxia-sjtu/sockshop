docker network create mynet

docker run -d --network mynet --name session-db redis:alpine
docker run -d -p 38079:8079 -e SESSION_REDIS=1 --network mynet --name frontend --cpus 0.5 dplsming/sockshop-frontend:0.1
docker run -d --network mynet --name carts-db mongo:3.4
docker run -d --network mynet --name user-db mongo:3.4
docker run -d --network mynet --name orders-db mongo:3.4
docker run -d --network mynet --name catalogue-db dplsming/sockshop-cataloguedb:0.1
docker run -d --network mynet --name catalogue weaveworksdemos/catalogue:0.3.5
docker run -d --network mynet --name payment weaveworksdemos/payment:0.4.3
docker run -d --network mynet --name shipping weaveworksdemos/shipping:0.4.8
docker run -d --network mynet --name carts weaveworksdemos/carts:0.4.8
docker run -d --network mynet --name user -e MONGO_HOST=user-db:27017 weaveworksdemos/user:0.4.4
docker run -d --network mynet --name orders weaveworksdemos/orders:0.4.7
docker run -d --network mynet --name frontend-1 -e SESSION_REDIS=1 --cpus 0.5 dplsming/sockshop-frontend:0.1
docker run -d --network mynet --name frontend-2 -e SESSION_REDIS=1 --cpus 0.5 dplsming/sockshop-frontend:0.1
docker run -d --name nginx-lb -p 29090:80 --network mynet registry.cn-shanghai.aliyuncs.com/wangtianxia/cse-handson3-nginx:v2.0
