docker stop carts-db && docker rm carts-db
docker stop user-db && docker rm user-db
docker stop orders-db && docker rm orders-db
docker stop catalogue && docker rm catalogue
docker stop payment && docker rm payment
docker stop carts && docker rm carts
docker stop orders && docker rm orders
docker stop user && docker rm user
docker stop shipping && docker rm shipping
docker stop frontend && docker rm frontend
docker stop catalogue-db && docker rm catalogue-db
docker stop nginx-frontend && docker rm nginx-frontend
docker network rm mynet
