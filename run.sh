docker run -d --name catalogue-db dplsming/sockshop-cataloguedb:0.1
docker run -d --name object-db mongo:3.4

docker network create mynet
docker run -d -p 38079:8079 --network mynet --name frontend dplsming/sockshop-frontend:0.1
docker run -d --network mynet --name carts-db mongo:3.4
docker run -d --network mynet --name user-db mongo:3.4
docker run -d --network mynet --name orders-db mongo:3.4
docker run -d --network mynet --name catalogue-db dplsming/sockshop-cataloguedb:0.1
docker run -d --network mynet --name catalogue weaveworksdemos/catalogue:0.3.5
docker run -d --network mynet --name payment weaveworksdemos/payment:0.4.3
docker run -d --network mynet --name shipping weaveworksdemos/shipping:0.4.8
docker run -d --network mynet --name carts weaveworksdemos/carts:0.4.8
docker run -d --network mynet --name user -e MONGO_HOST=user-db:27017 weaveworksdemos/user:0.4.4
docker run -d --network mynet --name orders weaveworksdemos/orders:0.4.7

docker stop carts-db && docker rm carts-db
docker stop user-db && docker rm user-db
docker stop orders-db && docker rm orders-db
docker stop catalogue && docker rm catalogue
docker stop payment && docker rm payment
docker stop carts && docker rm carts
docker stop orders && docker rm orders
docker stop user && docker rm user
docker stop shipping && docker rm shipping
docker stop frontend && docker rm frontend
docker stop catalogue-db && docker rm catalogue-db


docker pull dplsming/sockshop-frontend:0.1
docker pull mongo:3.4
docker pull weaveworksdemos/catalogue:0.3.5
docker pull weaveworksdemos/payment:0.4.3
docker pull weaveworksdemos/carts:0.4.8
docker pull weaveworksdemos/orders:0.4.7
docker pull weaveworksdemos/user:0.4.4
docker pull weaveworksdemos/shipping:0.4.8
docker pull dplsming/sockshop-cataloguedb:0.1
docker pull dplsming/sockshop-test:0.1

docker tag dplsming/sockshop-frontend:0.1  registry-vpc.cn-shanghai.aliyuncs.com/sockshop/sockshop-frontend:0.1
docker tag mongo:3.4 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/mongo:3.4
docker tag weaveworksdemos/catalogue:0.3.5 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/catalogue:0.3.5
docker tag weaveworksdemos/payment:0.4.3 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/payment:0.4.3
docker tag weaveworksdemos/carts:0.4.8 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/carts:0.4.8
docker tag weaveworksdemos/orders:0.4.7 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/orders:0.4.7
docker tag weaveworksdemos/user:0.4.4 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/user:0.4.4
docker tag weaveworksdemos/shipping:0.4.8 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/shipping:0.4.8
docker tag dplsming/sockshop-cataloguedb:0.1 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/sockshop-cataloguedb:0.1
docker tag dplsming/sockshop-test:0.1 registry-vpc.cn-shanghai.aliyuncs.com/sockshop/sockshop-test:0.1

docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/sockshop-frontend:0.1
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/mongo:3.4
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/catalogue:0.3.5
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/payment:0.4.3
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/carts:0.4.8
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/orders:0.4.7
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/user:0.4.4
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/shipping:0.4.8
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/sockshop-cataloguedb:0.1
docker push registry-vpc.cn-shanghai.aliyuncs.com/sockshop/sockshop-test:0.1

docker pull registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-frontend:0.1
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/mongo:3.4
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/catalogue:0.3.5
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/payment:0.4.3
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/carts:0.4.8
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/orders:0.4.7
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/user:0.4.4
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/shipping:0.4.8
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-cataloguedb:0.1
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-test:0.1

docker tag registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-frontend:0.1 dplsming/sockshop-frontend:0.1
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/mongo:3.4 mongo:3.4
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/catalogue:0.3.5 weaveworksdemos/catalogue:0.3.5
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/payment:0.4.3 weaveworksdemos/payment:0.4.3
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/carts:0.4.8 weaveworksdemos/carts:0.4.8
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/orders:0.4.7 weaveworksdemos/orders:0.4.7
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/user:0.4.4 weaveworksdemos/user:0.4.4
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/shipping:0.4.8 weaveworksdemos/shipping:0.4.8
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-cataloguedb:0.1 dplsming/sockshop-cataloguedb:0.1
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-test:0.1 dplsming/sockshop-test:0.1

docker network inspect mynet
docker run --network mynet -it --rm dplsming/sockshop-test:0.1 http://172.18.0.1:38079 correctness
