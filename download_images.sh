docker pull registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-frontend:0.1
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/mongo:3.4
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/catalogue:0.3.5
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/payment:0.4.3
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/carts:0.4.8
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/orders:0.4.7
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/user:0.4.4
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/shipping:0.4.8
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-cataloguedb:0.1
docker pull registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-test:0.1

docker tag registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-frontend:0.1 dplsming/sockshop-frontend:0.1
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/mongo:3.4 mongo:3.4
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/catalogue:0.3.5 weaveworksdemos/catalogue:0.3.5
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/payment:0.4.3 weaveworksdemos/payment:0.4.3
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/carts:0.4.8 weaveworksdemos/carts:0.4.8
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/orders:0.4.7 weaveworksdemos/orders:0.4.7
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/user:0.4.4 weaveworksdemos/user:0.4.4
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/shipping:0.4.8 weaveworksdemos/shipping:0.4.8
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-cataloguedb:0.1 dplsming/sockshop-cataloguedb:0.1
docker tag registry.cn-shanghai.aliyuncs.com/sockshop/sockshop-test:0.1 dplsming/sockshop-test:0.1


