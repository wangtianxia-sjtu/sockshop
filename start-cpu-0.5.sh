docker network create mynet
docker run -d -p 38079:8079 --network mynet --name frontend --cpus 0.5 dplsming/sockshop-frontend:0.1
docker run -d --network mynet --name carts-db mongo:3.4
docker run -d --network mynet --name user-db mongo:3.4
docker run -d --network mynet --name orders-db mongo:3.4
docker run -d --network mynet --name catalogue-db dplsming/sockshop-cataloguedb:0.1
docker run -d --network mynet --name catalogue weaveworksdemos/catalogue:0.3.5
docker run -d --network mynet --name payment weaveworksdemos/payment:0.4.3
docker run -d --network mynet --name shipping weaveworksdemos/shipping:0.4.8
docker run -d --network mynet --name carts weaveworksdemos/carts:0.4.8
docker run -d --network mynet --name user -e MONGO_HOST=user-db:27017 weaveworksdemos/user:0.4.4
docker run -d --network mynet --name orders weaveworksdemos/orders:0.4.7
docker run -d -p 39090:80 --network mynet --name nginx-frontend registry.cn-shanghai.aliyuncs.com/wangtianxia/cse-handson3-nginx:v1.0
